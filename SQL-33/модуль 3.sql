--=============== МОДУЛЬ 3. ОСНОВЫ SQL =======================================
--= ПОМНИТЕ, ЧТО НЕОБХОДИМО УСТАНОВИТЬ ВЕРНОЕ СОЕДИНЕНИЕ И ВЫБРАТЬ СХЕМУ PUBLIC===========
SET search_path TO 'dvd-rental';

--======== ОСНОВНАЯ ЧАСТЬ ==============

--ЗАДАНИЕ №1
--Выведите для каждого покупателя его адрес проживания,
--город и страну проживания.
select concat(c.last_name, ' ', c.first_name) "Фамилия и имя", a.address Адрес, ct.city Город, cntr.country Страна
  from customer c
 inner join address a on a.address_id = c.address_id
 inner join city ct on ct.city_id  = a.city_id
 inner join country cntr on cntr.country_id = ct.country_id;


--ЗАДАНИЕ №2
--С помощью SQL-запроса посчитайте для каждого магазина количество его покупателей.
select store_id "ID магазина", count(*) "Количество покупателей"
 from customer c
group by store_id;


--Доработайте запрос и выведите только те магазины,
--у которых количество покупателей больше 300-от.
--Для решения используйте фильтрацию по сгруппированным строкам
--с использованием функции агрегации.
select store_id "ID магазина", count(*) "Количество покупателей"
 from customer c
group by store_id
having count(*) > 300;


-- Доработайте запрос, добавив в него информацию о городе магазина,
--а также фамилию и имя продавца, который работает в этом магазине.
select s.store_id "ID магазина", mt.total_sales "Количество покупателей", ct.city "Город магазина", concat(st.last_name, ' ', st.first_name) "Фамилия и имя продавца"
  from store s
  inner join (select store_id, count(*) total_sales
               from customer c
           group by store_id
             having count(*) > 300) mt on mt.store_id = s.store_id
  inner join address a on a.address_id = s.address_id
  inner join city ct on ct.city_id = a.city_id
  inner join staff st on st.store_id =s.store_id


--ЗАДАНИЕ №3
--Выведите ТОП-5 покупателей,
--которые взяли в аренду за всё время наибольшее количество фильмов
select concat(c.last_name, ' ', c.first_name) "Фамилия и имя покупателя", t.total_rented "Количество фильмов"
  from customer c
 inner join (select customer_id, count(*) total_rented
               from rental r
              group by customer_id
              order by 2 desc
              limit 5) t on t.customer_id = c.customer_id
order by 2 desc


--ЗАДАНИЕ №4
--Посчитайте для каждого покупателя 4 аналитических показателя:
--  1. количество фильмов, которые он взял в аренду
--  2. общую стоимость платежей за аренду всех фильмов (значение округлите до целого числа)
--  3. минимальное значение платежа за аренду фильма
--  4. максимальное значение платежа за аренду фильма
with mt as
(select c.customer_id, count(r.*) total_rents, round(sum(amount)) total_paid, min(amount) min_paid, max(amount) max_paid
  from customer c
 inner join rental r on r.customer_id =c.customer_id
  left join payment p on p.rental_id = r.rental_id -- для 1452 записей в rental нет соответствующей записи в payment
 group by c.customer_id
 )

select concat(c.last_name, ' ', c.first_name) "Фамилия и имя покупателя",
       total_rents "Количество фильмов",
       total_paid "Общая стоимость платежей",
       min_paid "Минимальная стоимость платежа",
       max_paid "Максимальная стоимость платежа"
  from customer c
 inner join mt on mt.customer_id = c.customer_id;


--ЗАДАНИЕ №5
--Используя данные из таблицы городов составьте одним запросом всевозможные пары городов таким образом,
 --чтобы в результате не было пар с одинаковыми названиями городов.
 --Для решения необходимо использовать декартово произведение.
select c1.city "Город 1", c2.city "Город 2"
  from city c1,
       city c2
 where c1.city_id != c2.city_id;


--ЗАДАНИЕ №6
--Используя данные из таблицы rental о дате выдачи фильма в аренду (поле rental_date)
--и дате возврата фильма (поле return_date),
--вычислите для каждого покупателя среднее количество дней, за которые покупатель возвращает фильмы.
select customer_id "ID покупателя", round(avg(return_date::date - rental_date::date),2) "Среднее количество дней на возврат"
  from rental r
 group by customer_id
 order by 1;


--======== ДОПОЛНИТЕЛЬНАЯ ЧАСТЬ ==============

--ЗАДАНИЕ №1
--Посчитайте для каждого фильма сколько раз его брали в аренду и значение общей стоимости аренды фильма за всё время.
with mt as (
  select f.film_id, count(f.*) "total_rents", sum(f.rental_rate * ceil(extract(epoch from return_date - rental_date) / 60 / 60 / 24)) "total_rent_sum"
  from film f
 inner join inventory i on i.film_id = f.film_id
 inner join rental r on r.inventory_id = i.inventory_id
 group by f.film_id, f.title
)

select f.title Название, f.rating Рейтинг, c."name" Жанр, f.release_year "Год выпуска", l."name" Язык,  mt.total_rents "Количество аренд", mt.total_rent_sum "Общая стоимость аренды"
  from film f
  inner join mt on mt.film_id = f.film_id
 inner join film_category fc on fc.film_id = f.film_id
 inner join category c on c.category_id = fc.category_id
 inner join "language" l on l.language_id = f.language_id
 order by 1;


--ЗАДАНИЕ №2
--Доработайте запрос из предыдущего задания и выведите с помощью запроса фильмы, которые ни разу не брали в аренду.
with mt as (
  select f.film_id, count(r.*) "total_rents", sum(f.rental_rate * ceil(extract(epoch from return_date - rental_date) / 60 / 60 / 24)) "total_rent_sum"
  from film f
 left join inventory i on i.film_id = f.film_id
 left join rental r on r.inventory_id = i.inventory_id
 group by f.film_id, f.title
)

select f.title Название, f.rating Рейтинг, c."name" Жанр, f.release_year "Год выпуска", l."name" Язык,  mt.total_rents "Количество аренд", mt.total_rent_sum "Общая стоимость аренды"
  from film f
  inner join mt on mt.film_id = f.film_id
 inner join film_category fc on fc.film_id = f.film_id
 inner join category c on c.category_id = fc.category_id
 inner join "language" l on l.language_id = f.language_id
 where mt.total_rents = 0
 order by 1;


--ЗАДАНИЕ №3
--Посчитайте количество продаж, выполненных каждым продавцом. Добавьте вычисляемую колонку "Премия".
--Если количество продаж превышает 7300, то значение в колонке будет "Да", иначе должно быть значение "Нет".
select staff_id "ID сотрудника", case
                   when count(payment_id) > 7300 then 'Да'
                   else 'Нет'
                 end "Премия"
  from payment p
 group by staff_id;
