-- Диплом

-- 1
-- В каких городах больше одного аэропорта?
select city, count(*)
  from airports a2
 group by city
having count(*) > 1;


-- 2
-- В каких аэропортах есть рейсы, выполняемые самолетом с максимальной дальностью перелета?
-- смотрим и на отправление и на прибытие. Теоретически, возможна ситуация, когда рейс с возвращается из аэропорта прибытия не напрямую обратно в аэропорт отправления, а через некую промежуточную точку
-- explain analyze cost 62.92
select r.departure_airport, r.departure_airport_name
  from aircrafts a
  join routes r on r.aircraft_code  = a.aircraft_code
 where a."range" = (select max("range") from aircrafts a2)
union
select r.arrival_airport, r.arrival_airport_name
  from aircrafts a
  join routes r on r.aircraft_code  = a.aircraft_code
 where a."range" = (select max("range") from aircrafts a2);

-- 3
-- Вывести 10 рейсов с максимальным временем задержки вылета
-- explain analyze 1057.12
select f.flight_id, f.flight_no, f.scheduled_departure, f.scheduled_arrival, f.departure_airport, f.arrival_airport, f.status, f.actual_departure, f.actual_arrival,
       actual_departure - scheduled_departure "Время задержки вылета" from flights f
 where status not in ('Delayed', 'Scheduled', 'On Time') -- у этих статусов не может быть actual_departure, потому можно их отсеить, чтобы ускорить сортировку
   and actual_departure is not null
 order by 10 desc
 limit 10;
-- если мы хотим видеть картину online в какой-нибудь view, можно использовать now() - scheduled_departure


-- 4
-- Были ли брони, по которым не были получены посадочные талоны?
-- explain analyze -- 25473
select count(b.*)
  from bookings b
  join tickets t on t.book_ref = b.book_ref
  left join boarding_passes bp on bp.ticket_no = t.ticket_no
 where bp.flight_id is null;



-- 5
-- Найдите свободные места для каждого рейса, их % отношение к общему количеству мест в самолете.
-- Добавьте столбец с накопительным итогом - суммарное накопление количества вывезенных пассажиров из каждого аэропорта на каждый день. Т.е. в этом столбце должна отражаться накопительная сумма - сколько человек уже вылетело из данного аэропорта на этом или более ранних рейсах за день.
-- explain analyze --14116
with seats_available as  -- собрать в CTE информацию по местам на каждом типе самолёта
(select count(*) total_seats_available, aircraft_code
  from seats s
 group by aircraft_code),
seats_taken as  -- собрать в CTE информацию по посадочным талонам. Исходим из того, что все пассажиры, получившие посадочные талоны сели в самолёт и не потерялись в аэропорту
(select count(*) total_seats_taken, flight_id
  from boarding_passes bp
 group by flight_id)
select f.flight_id, f.flight_no, f.departure_airport, f.arrival_airport, f.status, f.aircraft_code,
  total_seats_available "Всего доступно мест", coalesce(total_seats_taken, 0) "Всего занято мест",
  total_seats_available - coalesce(total_seats_taken, 0) "Всего свободно мест",
  round(((total_seats_available - coalesce(total_seats_taken,0)::numeric) /total_seats_available::numeric * 100), 2) "Процент свободных мест",
  coalesce(sum(total_seats_taken) over(partition by f.departure_airport, f.actual_departure::date order by f.actual_departure), 0) "Суммарное накопление числа вывезенных пассажиров"
  from flights f
  join seats_available sa on f.aircraft_code = sa.aircraft_code
  left join seats_taken st on f.flight_id = st.flight_id
 where f.status not in ('Delayed', 'Scheduled', 'On Time', 'Cancelled')


-- 6
-- Найдите процентное соотношение перелетов по типам самолетов от общего количества.
-- explain analyze --1695
 select aircraft_code, round(count(*)::numeric /(select count(*)::numeric from flights) * 100, 2)
  from flights f
 group by aircraft_code;

-- детальнее:
-- explain analyze --1696
with flight_stats as
(select aircraft_code, round(count(*)::numeric /(select count(*)::numeric from flights) * 100, 2) prcnt
  from flights f
 group by aircraft_code)
select a.aircraft_code, model, prcnt
  from aircrafts a
  join flight_stats fs on a.aircraft_code = fs.aircraft_code
 order by 3 desc;


-- 7
-- Были ли города, в которые можно  добраться бизнес - классом дешевле, чем эконом-классом в рамках перелета?
-- explain analyze --102194
with mt1 as  -- самый дешёвый бизнес-класс
(select flight_id, min(amount) amt
  from ticket_flights tf
 where fare_conditions = 'Business'
 group by flight_id),
mt2 as  -- самый дорогой эконом-класс
(select flight_id, max(amount) amt
  from ticket_flights tf
 where fare_conditions = 'Economy'
 group by flight_id)
select distinct f.departure_city, f.arrival_city, mt1.flight_id, mt1.amt, mt2.amt
  from mt1
  join mt2 on mt1.flight_id = mt2.flight_id
  join flights_v f on mt1.flight_id = f.flight_id
 where mt1.amt < mt2.amt;


-- 8
-- Между какими городами нет прямых рейсов?

-- возможные комбинации городов без пар A-B <=> B-A
create or replace view all_city_combinations as
select distinct a.city c1, a2.city c2
  from airports a
  join airports a2 on lower(a.city) < lower(a2.city)

-- explain analyze --423
select c1, c2 from all_city_combinations
 except
select departure_city, arrival_city from routes;

-- Тут важный момент: мы исходим из того, что рейсы строятся парами: то есть если есть рейс A -> B, то точно есть рейс B -> A
-- Справедливость этого можно проверить пустотой селекта
/*
select * from (
select departure_city, arrival_city from routes
except
select arrival_city, departure_city from routes
) t1
union
select * from (
select arrival_city, departure_city from routes
except
select departure_city, arrival_city from routes
) t2
*/


-- 9
-- Вычислите расстояние между аэропортами, связанными прямыми рейсами, сравните с допустимой максимальной дальностью перелетов  в самолетах, обслуживающих эти рейсы *

-- функция для вычисления расстояния
create or replace function f_calc_distance_by_coords(lat1 in airports.latitude%type, lat2 in airports.latitude%type, lon1 in airports.longitude%type, lon2 in airports.longitude%type)
 returns numeric as $$
 declare
  earth_radius numeric := 6371; --in km
begin
   return  acos(sind(lat1) * sind(lat2) + cosd(lat1) * cosd(lat2) * cosd(lon1 - lon2)) * earth_radius;
end;
$$ LANGUAGE plpgsql;

--explain analyze --292
select distinct on(distance) flight_no, departure_airport, departure_airport_name, arrival_airport, arrival_airport_name, model,
       distance "Расстояние между аэропортами",
       "range" "Дальность полёта",
       case when range - distance < distance * 0.15 then '!!!Перелёт с менее чем 15% запасом!!!'
            when range - distance between distance * 0.15 and distance * 0.3 then 'Перелёт с соблюдением минимально рекомендованного запаса'
            when range - distance > distance * 0.3 then 'Перелёт с более чем 30% запаса'
        end
from (
select r.flight_no, r.departure_airport, r.departure_airport_name, r.arrival_airport, r.arrival_airport_name, a.model,
       round(f_calc_distance_by_coords(a1.latitude, a2.latitude, a1.longitude, a2.longitude), 2) distance,
       a."range"
  from routes r
  join airports a1 on r.departure_airport = a1.airport_code
  join airports a2 on r.arrival_airport = a2.airport_code
  join aircrafts a on r.aircraft_code = a.aircraft_code
) t
order by distance, flight_no

/*
  в США рекомендованый минимальный запас дальности полёта составляет 115% от расстояния между объектами
  https://nbaa.org/wp-content/uploads/2018/01/safo06012.pdf
  Аналогичные рекомендации для РФ найти не удалось, но, можно предположить, что они буду примерно такими же
*/
