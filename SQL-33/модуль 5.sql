--=============== МОДУЛЬ 5. РАБОТА С POSTGRESQL =======================================
--= ПОМНИТЕ, ЧТО НЕОБХОДИМО УСТАНОВИТЬ ВЕРНОЕ СОЕДИНЕНИЕ И ВЫБРАТЬ СХЕМУ PUBLIC===========
SET search_path TO 'dvd-rental';

--======== ОСНОВНАЯ ЧАСТЬ ==============

--ЗАДАНИЕ №1
--Сделайте запрос к таблице payment и с помощью оконных функций добавьте вычисляемые колонки согласно условиям:
--	Пронумеруйте все платежи от 1 до N по дате
--	Пронумеруйте платежи для каждого покупателя, сортировка платежей должна быть по дате
--	Посчитайте нарастающим итогом сумму всех платежей для каждого покупателя, сортировка должна быть сперва по дате платежа, а затем по сумме платежа от наименьшей к большей
--	Пронумеруйте платежи для каждого покупателя по стоимости платежа от наибольших к меньшим так, чтобы платежи с одинаковым значением имели одинаковое значение номера.
-- Можно составить на каждый пункт отдельный SQL-запрос, а можно объединить все колонки в одном запросе.
select customer_id, payment_id, payment_date,
       row_number () over (order by payment_date) RN_DATE,
       row_number () over (partition by customer_id order by payment_date) RN_CUST_DATE,
       sum(amount) over (partition by customer_id order by payment_date, amount) sum_by_cust_inc,
       dense_rank () over (partition by customer_id order by amount desc) rank_by_cust_amt
  from payment p
 order by customer_id, payment_id ;


--ЗАДАНИЕ №2
-- С помощью оконной функции выведите для каждого покупателя стоимость платежа
-- и стоимость платежа из предыдущей строки со значением по умолчанию 0.0 с сортировкой по дате.
select customer_id, payment_id, payment_date, amount,
       lag(p.amount, 1, 0.) over (partition by customer_id order by payment_date) last_amount
  from payment p;


--ЗАДАНИЕ №3
-- С помощью оконной функции определите, на сколько каждый
-- следующий платеж покупателя больше или меньше текущего.
select customer_id, payment_id, payment_date, amount,
       amount - lead(p.amount, 1, 0.) over (partition by customer_id order by payment_date) difference
  from payment p;


 --ЗАДАНИЕ №4
-- С помощью оконной функции для каждого покупателя выведите данные о его последней оплате аренды.
select customer_id, payment_id, payment_date, amount from (
 select p.*, row_number () over (partition by customer_id order by payment_date desc) rn from payment p) t
 where rn = 1


--======== ДОПОЛНИТЕЛЬНАЯ ЧАСТЬ ==============

--ЗАДАНИЕ №1
--С помощью оконной функции выведите для каждого сотрудника сумму продаж за март 2007 года
-- с нарастающим итогом по каждому сотруднику и
-- по каждой дате продажи (без учёта времени) с сортировкой по дате.
select staff_id, payment_date::date, sum(amount) sum_amount, sum(sum(amount)) over (partition by staff_id order by payment_date::date)
  from payment p
 where date_trunc('month', payment_date) = '01.03.2007'
 group by staff_id, payment_date::date
 order by staff_id, payment_date;


--ЗАДАНИЕ №2
--10 апреля 2007 года в магазинах проходила акция: покупатель, совершивший каждый 100ый платеж
-- получал дополнительную скидку на следующую аренду.
-- С помощью оконной функции выведите всех покупателей, которые в день проведения акции получили скидку.
select customer_id, payment_id, payment_date, payment_number from (
  select p.*, row_number () over (order by payment_date) payment_number from payment p
   where date_trunc('day', payment_date) = '10.04.2007') t
where payment_number % 100 = 0;


--ЗАДАНИЕ №3
--Для каждой страны определите и выведите одним SQL-запросом покупателей, которые попадают под условия:
-- 1. покупатель, арендовавший наибольшее количество фильмов
-- 2. покупатель, арендовавший фильмов на самую большую сумму
-- 3. покупатель, который последним арендовал фильм
with mt as (
select country_id, t.customer_id, concat(c.first_name, ' ', c.last_name) nm,
       row_number () over(partition by country_id order by count_rents desc) rn1,
       row_number () over(partition by country_id order by sum_amt desc) rn2,
       row_number () over(partition by country_id order by max_date desc) rn3
  from (
  select cntr.country_id, cust.customer_id,
         count(r.*) count_rents,
         sum(p.amount) sum_amt,
         max(r.rental_date) max_date
    from customer cust
   inner join address addr on addr.address_id = cust.address_id
   inner join city ct on ct.city_id = addr.city_id
   inner join country cntr on cntr.country_id = ct.country_id
   inner join rental r on r.customer_id = cust.customer_id
   inner join payment p on p.rental_id = r.rental_id
   group by cust.customer_id, cntr.country_id
   ) t
  inner join customer c on c.customer_id = t.customer_id
 )

select cntr.country, mt1.nm, mt2.nm, mt3.nm
  from country cntr
 inner join mt mt1 on mt1.country_id = cntr.country_id and mt1.rn1 = 1
 inner join mt mt2 on mt2.country_id = cntr.country_id and mt2.rn2 = 1
 inner join mt mt3 on mt3.country_id = cntr.country_id and mt3.rn3 = 1;
