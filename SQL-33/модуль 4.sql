--=============== МОДУЛЬ 4. УГЛУБЛЕНИЕ В SQL =======================================
--= ПОМНИТЕ, ЧТО НЕОБХОДИМО УСТАНОВИТЬ ВЕРНОЕ СОЕДИНЕНИЕ И ВЫБРАТЬ СХЕМУ PUBLIC===========
SET search_path TO public;

--======== ОСНОВНАЯ ЧАСТЬ ==============

--ЗАДАНИЕ №1
--База данных: если подключение к облачной базе, то создаете новые таблицы в формате:
--таблица_фамилия,
--если подключение к контейнеру или локальному серверу, то создаете новую схему и в ней создаете таблицы.

create schema lecture_4;

set search_path to lecture_4;


-- Спроектируйте базу данных для следующих сущностей:
-- 1. язык (в смысле английский, французский и тп)
-- 2. народность (в смысле славяне, англосаксы и тп)
-- 3. страны (в смысле Россия, Германия и тп)



--Правила следующие:
-- на одном языке может говорить несколько народностей
-- одна народность может входить в несколько стран
-- каждая страна может состоять из нескольких народностей


--Требования к таблицам-справочникам:
-- идентификатор сущности должен присваиваться автоинкрементом
-- наименования сущностей не должны содержать null значения и не должны допускаться дубликаты в названиях сущностей

--СОЗДАНИЕ ТАБЛИЦЫ ЯЗЫКИ
create table lang (
  id serial primary key,
  name varchar(150) not null unique
);


--ВНЕСЕНИЕ ДАННЫХ В ТАБЛИЦУ ЯЗЫКИ
insert into lang("name") values
 ('Китайский'),
 ('Испанский'),
 ('Английский'),
 ('Арабский'),
 ('Хинди'),
 ('Бенгальский'),
 ('Португальский'),
 ('Русский'),
 ('Японский');

commit;

--СОЗДАНИЕ ТАБЛИЦЫ НАРОДНОСТИ
create table nation (
  id serial primary key,
  name varchar(500) not null unique
);

--ВНЕСЕНИЕ ДАННЫХ В ТАБЛИЦУ НАРОДНОСТИ
insert into nation (name) values
 ('Ханьцы'),
 ('Чжуаны'),
 ('Маньчжуры'),
 ('Хуэй'),
 ('Сингапурцы'),
 ('Тайваньцы'),
 ('Испанцы'),
 ('Мексиканцы'),
 ('Кубинцы'),
 ('Доминиканцы'),
 ('Англосаксы'),
 ('Новозеландцы'),
 ('Арабы'),
 ('Индийцы'),
 ('Бангладешцы'),
 ('Португальцы'),
 ('Бразильцы'),
 ('Ангольцы'),
 ('Славяне'),
 ('Японцы');

commit;


--СОЗДАНИЕ ТАБЛИЦЫ СТРАНЫ
create table country (
  id serial primary key,
  name varchar(500) not null unique
);

--ВНЕСЕНИЕ ДАННЫХ В ТАБЛИЦУ СТРАНЫ
insert into country (name) values
 ('Китай'),
 ('Сингапур'),
 ('Тайвань'),
 ('Египет'),
 ('Сирия'),
 ('Алжир'),
 ('Испания'),
 ('Мексика'),
 ('Куба'),
 ('Доминикана'),
 ('Великобритания'),
 ('Новая Зеландия'),
 ('Индия'),
 ('Бангладеш'),
 ('Португалия'),
 ('Бразилия'),
 ('Ангола'),
 ('Россия'),
 ('Белоруссия'),
 ('Казахстан'),
 ('Япония');

commit;

--СОЗДАНИЕ ПЕРВОЙ ТАБЛИЦЫ СО СВЯЗЯМИ
create table country_population (
  country_id smallint not null references country(id),
  nation_id  smallint not null references nation(id),
  constraint country_population_pk primary key (country_id, nation_id)
);


--ВНЕСЕНИЕ ДАННЫХ В ТАБЛИЦУ СО СВЯЗЯМИ
insert into country_population (country_id, nation_id) values
 (1, 1),
 (1, 2),
 (1, 3),
 (1, 4),
 (2, 5),
 (3, 6),
 (4, 13),
 (5, 13),
 (6, 13),
 (7, 7),
 (8, 8),
 (9, 9),
 (10, 10),
 (11, 11),
 (12, 12),
 (13, 14),
 (13, 15),
 (14, 14),
 (14, 15),
 (15, 16),
 (16, 17),
 (17, 18),
 (18, 19),
 (19, 19),
 (20, 19),
 (21, 20);

commit;


--СОЗДАНИЕ ВТОРОЙ ТАБЛИЦЫ СО СВЯЗЯМИ
create table nation_languages(
  nation_id smallint not null references nation(id),
  language_id smallint not null references lang(id),
  constraint nation_languages_pk primary key (nation_id, language_id)
);


--ВНЕСЕНИЕ ДАННЫХ В ТАБЛИЦУ СО СВЯЗЯМИ
insert into nation_languages (nation_id, language_id) values
 (1, 1),
 (2, 1),
 (3, 1),
 (4, 1),
 (5, 1),
 (6, 1),
 (7, 2),
 (8, 2),
 (9, 2),
 (10, 2),
 (11, 3),
 (12, 3),
 (13, 4),
 (14, 5),
 (15, 5),
 (15, 6),
 (16, 7),
 (17, 7),
 (18, 7),
 (19, 8),
 (20, 9);

commit;


-- языки и народности в стране
select c.name, n.name, l.name
  from country c
  inner join country_population cp on cp.country_id = c.id
  inner join nation_languages nl on nl.nation_id = cp.nation_id
  inner join nation n on n.id = cp.nation_id
  inner join lang l on l.id = nl.language_id;

--======== ДОПОЛНИТЕЛЬНАЯ ЧАСТЬ ==============


--ЗАДАНИЕ №1
--Создайте новую таблицу film_new со следующими полями:
--·   	film_name - название фильма - тип данных varchar(255) и ограничение not null
--·   	film_year - год выпуска фильма - тип данных integer, условие, что значение должно быть больше 0
--·   	film_rental_rate - стоимость аренды фильма - тип данных numeric(4,2), значение по умолчанию 0.99
--·   	film_duration - длительность фильма в минутах - тип данных integer, ограничение not null и условие, что значение должно быть больше 0
--Если работаете в облачной базе, то перед названием таблицы задайте наименование вашей схемы.
create table film_new (
  film_name varchar(255) not null,
  film_year integer check(film_year > 0),
  film_rental_rate numeric(4,2) default 0.99,
  film_duration integer not null check(film_duration > 0 )
  );


--ЗАДАНИЕ №2
--Заполните таблицу film_new данными с помощью SQL-запроса, где колонкам соответствуют массивы данных:
--·       film_name - array['The Shawshank Redemption', 'The Green Mile', 'Back to the Future', 'Forrest Gump', 'Schindlers List']
--·       film_year - array[1994, 1999, 1985, 1994, 1993]
--·       film_rental_rate - array[2.99, 0.99, 1.99, 2.99, 3.99]
--·   	  film_duration - array[142, 189, 116, 142, 195]
insert into film_new (film_name, film_year, film_rental_rate, film_duration)
  select unnest(array['The Shawshank Redemption', 'The Green Mile', 'Back to the Future', 'Forrest Gump', 'Schindlers List']),
         unnest(array[1994, 1999, 1985, 1994, 1993]),
         unnest(array[2.99, 0.99, 1.99, 2.99, 3.99]),
         unnest(array[142, 189, 116, 142, 195]);
commit;


--ЗАДАНИЕ №3
--Обновите стоимость аренды фильмов в таблице film_new с учетом информации,
--что стоимость аренды всех фильмов поднялась на 1.41
update film_new
   set film_rental_rate = film_rental_rate + 1.41;
commit;


--ЗАДАНИЕ №4
--Фильм с названием "Back to the Future" был снят с аренды,
--удалите строку с этим фильмом из таблицы film_new
delete from film_new
 where film_name = 'Back to the Future';
commit;

--ЗАДАНИЕ №5
--Добавьте в таблицу film_new запись о любом другом новом фильме
insert into film_new (film_name, film_year, film_rental_rate, film_duration) values
 ('Scott Pilgrim vs. the World', 2010, 2.99, 118);
commit;


--ЗАДАНИЕ №6
--Напишите SQL-запрос, который выведет все колонки из таблицы film_new,
--а также новую вычисляемую колонку "длительность фильма в часах", округлённую до десятых
select t.*, round(film_duration / 60::numeric, 1) from film_new t


--ЗАДАНИЕ №7
--Удалите таблицу film_new
drop table film_new;
